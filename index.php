
<!DOCTYPE html>
<html>
  <head>
    <title>JavaScript Chess Game</title>
    <meta charset="utf-8">
    <meta name="desciption" content="bla">
    <meta name="author" content="Hendrik Herpers">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/style.css">
  </head>
  <body>
	<header>
		<h1>Schachspiel</h1>
		<h2>by<br/>Hendrik Herpers</h2>

	</header>
	<main>
		<h3>Willkommen bei meinem selbstgebauten Schachspiel!</h3>
		<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam</p>

		<div class="chess-plate">
			<table class="js-chessField">
				
				<tr class="chess-nav">
					<td class="turn"></td>
					<td class="turn">1</td>
					<td class="turn">2</td>
					<td class="turn">3</td>
					<td class="turn">4</td>
					<td class="turn">5</td>
					<td class="turn">6</td>
					<td class="turn">7</td>
					<td class="turn">8</td>
					<td class="turn"></td>
				</tr>
				<tr id="a">
					<td>a</td>
					<td class="js-field"><span class="js-figure" data-type="yr"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yh"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yb"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yq"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yk"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yb"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yh"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yr"></span></td>
					<td class="turn">a</td>
				</tr>
				<tr id="b">
					<td>b</td>
					<td class="js-field"><span class="js-figure" data-type="yp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="yp"></span></td>
					<td class="turn">b</td>
				</tr>
				<tr id="c">
					<td>c</td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="turn">c</td>
				</tr>
				<tr id="d">
					<td>d</td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="turn">d</td>
				</tr>
				<tr id="e">
					<td>e</td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="turn">e</td>
				</tr>
				<tr id="f">
					<td>f</td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="js-field"></td>
					<td class="turn">f</td>
				</tr>
				<tr id="g">
					<td>g</td>
					<td class="js-field"><span class="js-figure" data-type="xp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xp"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xp"></span></td>
					<td class="turn">g</td>
				</tr>
				<tr id="h">
					<td>h</td>
					<td class="js-field"><span class="js-figure" data-type="xr"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xh"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xb"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xk"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xq"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xb"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xh"></span></td>
					<td class="js-field"><span class="js-figure" data-type="xr"></span></td>
					<td class="turn">h</td>
				</tr>
				<tr class="chess-nav">
					<td></td>
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>
					<td>5</td>
					<td>6</td>
					<td>7</td>
					<td>8</td>
					<td></td>
				</tr>
				
			</table>
		</div>
		
		<div class="js-figureSelectOverlay">			
			<div class="choose-figure">
				<p>Wähle eine Figur</p>
				<br/>
				<a href="#" class="js-figureSelect" data-type="r">Rook</a>
				<a href="#" class="js-figureSelect" data-type="h">Knight</a>
				<a href="#" class="js-figureSelect" data-type="q">Queen</a>
				<a href="#" class="js-figureSelect" data-type="b">Bishop</a>
			</div>
		</div>
		
	</main>
	<footer>
		<h2></h2>
	</footer>
	
	<!-- JavaScript -->
	<script src="js/attributes.js"></script>

	<script src="js/classes.js"></script>

	<?php
		foreach (glob('js/objects/*.js') as $file) {
	?> 
		<script src="<?php echo $file; ?>"></script>
	<?php
		}
	?>

	<script src="js/action.js"></script>
	
  </body>
</html>
