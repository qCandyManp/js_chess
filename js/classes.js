class Field {
	constructor(htmlObj, id) {
		this.htmlObj = htmlObj;
		this.id = id;
		if (typeof this.htmlObj !== 'undefined') {
			this.posX = this.htmlObj.offsetLeft;
			this.posY = this.htmlObj.offsetTop; 
			this.valid = false;
			this.killable = false;
		}		
	}

	get getPosition () {
		//get the position of the valid field
		let fieldPos = [this.posX, this.posY];
		
		return fieldPos;
	}

	get setId () {
		this.htmlObj.setAttribute(dataId, this.id);
	}

	get setValid () {
		this.valid = 'true';

		if (this.valid = 'true') {
			this.htmlObj.classList.add(classActive);
		}
	}
}

class Figure {
	constructor(htmlObj) {
		this.htmlObj = htmlObj;
		
		if (typeof this.htmlObj !== 'undefined') {	
			this.fieldId = this.htmlObj.parentNode.getAttribute(dataId);
			this.fieldRowId = this.getFieldRowId;
			this.posX = this.htmlObj.offsetLeft;
			this.posY = this.htmlObj.offsetTop;
			this.width = this.htmlObj.offsetWidth;
			this.height = this.htmlObj.offsetHeight;
			this.type = this.htmlObj.getAttribute(dataType).toString().slice(1,2);
			this.color = this.htmlObj.getAttribute(dataType).toString().slice(0,1);
			this.moved = parseInt(this.htmlObj.getAttribute(dataMoved));
			this.validFieldIds;
			this.possibleKills;
			this.killed = false;
		}
	}

	get resetFigureInfo () {
		if (typeof this.htmlObj !== 'undefined') {	
			this.fieldId = this.htmlObj.parentNode.getAttribute(dataId);
			this.fieldRowId = this.getFieldRowId;
			this.posX = this.htmlObj.offsetLeft;
			this.posY = this.htmlObj.offsetTop;
			this.width = this.htmlObj.offsetWidth;
			this.height = this.htmlObj.offsetHeight;
			this.type = this.htmlObj.getAttribute(dataType).toString().slice(1,2);
			this.color = this.htmlObj.getAttribute(dataType).toString().slice(0,1);
		}
	}

	get getPosition () {
		//get the position of the figure
		let fieldPos = [this.posX, this.posY];
		
		return fieldPos;
	}

	get getFieldRowId () {
		let fieldRowId;
		let i;

		for(i = 0; i < arrFieldRows.length; i++) {
			arrFieldRows[i].forEach( (id) => {
				if (id == this.fieldId) {
					fieldRowId = i;
				}
			})
		}

		return fieldRowId;
	}

	setFieldId (id) {
		this.fieldId = id;

		this.htmlObj.setAttribute(dataId, id);
	}

	checkForFigures(id) {
		let output;
		let i;
		
		for (i = 0 ; i < arrFigures.length; i++) {
			if (arrFigures[i]) {
				if(arrFigures[i].fieldId != false && arrFigures[i].fieldId == id) {
					output = true;
					break;
				} else {
					output = false;
				}
				
			}
		}

		return output;
	}

	get checkForKills() {
		this.possibleKills = [];

		if (this.validFieldIds) {
			this.validFieldIds.forEach((validId, index) => {
				let i;
				if (validId) {
					for (i = 0 ; i < arrFigures.length; i++) {
						if (arrFigures[i]) {
							if (arrFigures[i].fieldId != false && arrFigures[i].fieldId == validId && arrFigures[i].color == this.color) {
								delete this.validFieldIds[index];
							} else if (arrFigures[i].fieldId != false && arrFigures[i].fieldId == validId && arrFigures[i].color !== this.color && !arrFigures[i].killed) {
								delete this.validFieldIds[index];
								this.possibleKills.push(validId);
							}
						}
					}
				}
			});
		}
	}

	calcFieldRowId(fieldId) {
		fieldId = parseInt(fieldId);
		let calcFieldRowId;
		let i;
		for (i = 0;i < arrFieldRows.length; ++i) {
			if (arrFieldRows[i].includes(fieldId)) {
				calcFieldRowId = i;
			}
		};
		
		return calcFieldRowId;
	}

	checkNewId(newId, rowId) {
		if (rowId < 0 || rowId > (arrFieldRows.length-1)) {
			return false;
		} else {
			if (arrFieldRows[rowId].includes(newId)) {
				return true;
			} else {
				return false;
			}
		}
	}

	validateVertical (limit, operator, arrValidIds) {
		parseInt(limit);
		let i;

		if (this.fieldId != false) {
			for (i = 1; i <= limit; i++) {
				let newId;

				if (operator == opPositive) {
					newId = parseInt(this.fieldId) + (i * 8);
				} else if (operator == opNegative) {
					newId = parseInt(this.fieldId) - (i * 8);
				}
				
				if (newId > arrFields.length || newId <= 0) {
					break;
				} else if (this.checkForFigures(newId)) {
					arrValidIds.push(newId);
					break;
				} else {
					arrValidIds.push(newId);
				}
			};
		}
	}

	validateHorizontal (limit, operator, arrValidIds) {
		parseInt(limit);
		let i;

		if (this.fieldId != false) {
			for (i = 1; i <= limit; i++) {
				let newId;

				if (operator == opPositive) {
					newId = parseInt(this.fieldId) + i;
				} else if (operator == opNegative) {
					newId = parseInt(this.fieldId) - i;
				}

				if (newId > arrFields.length || newId <= 0 || !arrFieldRows[this.fieldRowId].includes(newId)) {
					break;
				} else if (this.checkForFigures(newId)) {
					arrValidIds.push(newId);
					break;
				} else {
					arrValidIds.push(newId);
				}
			};
		}
	}

	validateDiagonal (limit, direction, arrValidIds) {
		parseInt(limit);
		let fieldId;
		let rowId = parseInt(this.fieldRowId);
		let i;

		if (this.fieldId != false) {
			for (i = 1; i <= limit; i++) {
				if (!fieldId) {
					fieldId = this.fieldId; 
				}
				
				rowId = this.calcFieldRowId(fieldId);

				if (direction == topLeft) {
					fieldId = parseInt(fieldId) + (8 + 1);
					++rowId;
				} else if (direction == topRight) {
					fieldId = parseInt(fieldId) + (8 - 1);
					++rowId;
				} else if (direction == bottomRight) {
					fieldId = parseInt(fieldId) - (8 - 1);
					--rowId;
				} else if (direction == bottomLeft) {
					fieldId = parseInt(fieldId) - (8 + 1);
					--rowId;
				}

				if (fieldId > arrFields.length ||fieldId <= 0 || !this.checkNewId(fieldId, rowId)) {
					break;
				} else if (this.checkForFigures(fieldId)) {
					arrValidIds.push(fieldId);
					break;
				} else {			
					arrValidIds.push(fieldId);
				}
			};
		}
	}
}