class Knight extends Figure {
	get validateFieldIds () {
		
		this.validFieldIds = [];

		let validIds = [];

		//rules
		let limit = 8;
		let i;

		if (this.fieldId != false) {
			for (i = 0; i < limit; i++) {
				let newId;
				let rowId = super.calcFieldRowId(this.fieldId);

				if (i == 0) {
					newId = parseInt(this.fieldId) + (8 + 2);
					rowId = parseInt(rowId) + 1;
				} else if (i == 1) {
					newId = parseInt(this.fieldId) + (8 * 2) + 1;
					rowId = parseInt(rowId) + 2;
				} else if (i == 2) {
					newId = parseInt(this.fieldId) + (8 * 2) - 1;
					rowId = parseInt(rowId) + 2;
				} else if (i == 3) {
					newId = parseInt(this.fieldId) + (8 - 2);
					rowId = parseInt(rowId) + 1;
				} else if (i == 4) {
					newId = parseInt(this.fieldId) - (8 + 2);
					rowId = parseInt(rowId) - 1;
				} else if (i == 5) {
					newId = parseInt(this.fieldId) - (8 * 2) + 1;
					rowId = parseInt(rowId) - 2;
				} else if (i == 6) {
					newId = parseInt(this.fieldId) - (8 * 2) - 1;
					rowId = parseInt(rowId) - 2;
				} else if (i == 7) {
					newId = parseInt(this.fieldId) - (8 - 2);
					rowId = parseInt(rowId) - 1;
				}

				if (newId <= arrFields.length && newId > 0 && arrFieldRows[rowId] && arrFieldRows[rowId].includes(newId)) {
					validIds.push(newId);
				}
			};
		}

		// set the valid ids after validation
		this.validFieldIds = validIds;
	}
}