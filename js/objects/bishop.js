class Bishop extends Figure {
	get validateFieldIds () {

		this.validFieldIds = [];

		let validIds = [];

		//rules
		super.validateDiagonal(8, topRight, validIds);
		super.validateDiagonal(8, topLeft, validIds);
		super.validateDiagonal(8, bottomRight, validIds);
		super.validateDiagonal(8, bottomLeft, validIds);

		// set the valid ids after validation
		this.validFieldIds = validIds;
	}
}