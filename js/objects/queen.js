class Queen extends Figure {
	get validateFieldIds () {

		this.validFieldIds = [];

		let validIds = [];

		//rules
		super.validateDiagonal(8, topRight, validIds);
		super.validateDiagonal(8, topLeft, validIds);
		super.validateDiagonal(8, bottomRight, validIds);
		super.validateDiagonal(8, bottomLeft, validIds);

		super.validateVertical(8, opNegative, validIds);
		super.validateVertical(8, opPositive, validIds);
		super.validateHorizontal(8, opNegative, validIds);
		super.validateHorizontal(8, opPositive, validIds);

		// set the valid ids after validation
		this.validFieldIds = validIds;
	}
}