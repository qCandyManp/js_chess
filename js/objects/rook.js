class Rook extends Figure {
	get validateFieldIds () {
		
		this.validFieldIds = [];

		let validIds = [];

		//rules
		super.validateVertical(8, opNegative, validIds);
		super.validateVertical(8, opPositive, validIds);
		
		super.validateHorizontal(8, opNegative, validIds);
		super.validateHorizontal(8, opPositive, validIds);

		// set the valid ids after validation
		this.validFieldIds = validIds;
	}
}