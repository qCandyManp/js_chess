class King extends Figure {
	constructor(htmlObj, id, rochadeIds) {
		super (htmlObj, id);

		this.rochadeIds = [];
	}

	get validateFieldIds () {
		
		let currentFig = this;

		this.validFieldIds = [];

		let validIds = [];

		//rules
		super.validateVertical(1, opNegative, validIds);
		super.validateVertical(1, opPositive, validIds);
		super.validateHorizontal(1, opNegative, validIds);
		super.validateHorizontal(1, opPositive, validIds);

		super.validateDiagonal(1, topRight, validIds);
		super.validateDiagonal(1, topLeft, validIds);
		super.validateDiagonal(1, bottomRight, validIds);
		super.validateDiagonal(1, bottomLeft, validIds);

		//--- Rochade
		let specialValidIds = [];

		if (this.moved == 0 || !this.moved) {

			function checkForRochade (validIds ,operator) {
				let newArray = [];
				let newId;
				let cornerIdsNegative = [1, 57];
				let cornerIdsPositive = [8, 64];

				validIds.forEach((id) => {
					let i;
					for (i = 0 ; i < arrFigures.length; i++) {
						if (arrFigures[i]) {
							if (arrFigures[i].fieldId == id) {
								if (arrFigures[i].type == typeRook && arrFigures[i].color == currentFig.color && !arrFigures[i].moved || arrFigures[i].type == typeRook && arrFigures[i].color == currentFig.color && arrFigures[i].moved <= 0) {
									if (operator == opPositive) {
										if (arrFigures[i].fieldId = cornerIdsPositive[0]) {
											newId = parseInt(currentFig.fieldId) + 2;
											newArray.push(newId);
										} else if (arrFigures[i].fieldId = cornerIdsPositive[1]) {
											newId = parseInt(currentFig.fieldId) + 2;
											newArray.push(newId);
										}
									} else if (operator == opNegative) {
										if (arrFigures[i].fieldId = cornerIdsNegative[0]) {
											newId = parseInt(currentFig.fieldId) - 2;
											newArray.push(newId);
										} else if (arrFigures[i].fieldId = cornerIdsNegative[1]) {
											newId = parseInt(currentFig.fieldId) - 2;
											newArray.push(newId);
										}
									}
								} else {
									newArray = [];
									break;
								}
							}
						}
					}
				});

				return newArray;
			}

			super.validateHorizontal(8, opPositive, specialValidIds);
			if (checkForRochade(specialValidIds ,opPositive)) {
				checkForRochade(specialValidIds ,opPositive).forEach((id) => {
					validIds.push(id);
					this.rochadeIds.push(id);
				});

				specialValidIds = [];
			}

			super.validateHorizontal(8, opNegative, specialValidIds);
			if (checkForRochade(specialValidIds ,opNegative)) {
				checkForRochade(specialValidIds ,opNegative).forEach((id) => {
					validIds.push(id);
					this.rochadeIds.push(id);
				});
			}
		}

		// set the valid ids after validation
		this.validFieldIds = validIds;
	}
}