class Pawn extends Figure {
	get validateFieldIds () {

		this.validFieldIds = [];

		let validIds = [];

		//rules

		//--- Black
		if(this.color == figBlack) {
			if (this.moved) {
				super.validateVertical(1, opPositive, validIds);
				super.validateDiagonal(1, topLeft, validIds);
				super.validateDiagonal(1, topRight, validIds);
			} else {
				super.validateVertical(2, opPositive, validIds);
				super.validateDiagonal(1, topLeft, validIds);
				super.validateDiagonal(1, topRight, validIds);
			}
		}

		//--- White
		if(this.color == figWhite) {
			if (this.moved) {
				super.validateVertical(1, opNegative, validIds);
				super.validateDiagonal(1, bottomLeft, validIds);
				super.validateDiagonal(1, bottomRight, validIds);
			} else {
				super.validateVertical(2, opNegative, validIds);
				super.validateDiagonal(1, bottomLeft, validIds);
				super.validateDiagonal(1, bottomRight, validIds);
			}
		}

		// set the valid ids after validation
		this.validFieldIds = validIds;
	}

	get checkForKills() {
		this.possibleKills = [];
		
		let fieldId = parseInt(this.fieldId);

		if (this.validFieldIds) {
			this.validFieldIds.forEach((validId, index) => {
				let i;
				for (i = 0 ; i < arrFigures.length; i++) {
					if (arrFigures[i] && arrFigures[i].fieldId != false) {
						if (arrFigures[i].fieldId == validId && arrFigures[i].color !== this.color) { 
							if (validId != (fieldId + 8) && validId != (fieldId + 16) && validId != (fieldId - 8) && validId != (fieldId - 16)) {
								delete this.validFieldIds[index];
								this.possibleKills.push(validId);
							} else {
								delete this.validFieldIds[index];
							}
						} else if (arrFigures[i].fieldId == validId && arrFigures[i].color == this.color) {
							delete this.validFieldIds[index];
						} else {
							if (validId != (fieldId + 8) && validId != (fieldId + 16) && validId != (fieldId - 8) && validId != (fieldId - 16)) {
								delete this.validFieldIds[index];
							}
						}
					}
				}
			});
		}
	}
}