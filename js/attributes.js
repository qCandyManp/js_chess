// class
let figureClass = 'js-figure';
let fieldClass = 'js-field';
let figureSelectOverlayClass = 'js-figureSelectOverlay';
let figureSelectClass = 'js-figureSelect';
let chessFieldClass = 'js-chessField';

let classValid = 'valid';
let classKillable = 'killable';
let classActive = 'active';

let classCssPawn = 'figure--pawn';
let classCssQueen = 'figure--queen';
let classCssKing = 'figure--king';
let classCssBishop = 'figure--bishop';
let classCssRook = 'figure--rook';
let classCssKnight = 'figure--knight';

let classCssFigureBlack = 'black';
let classCssFigureWhite = 'white';

let classCssTurn = 'turn';

// data
let dataId = 'data-fieldid';
let dataType = 'data-type';
let dataMoved = 'data-moved';
let dataPullColor = 'data-currentColor';

// figure types
let typePawn = 'p';
let typeRook = 'r';
let typeKnight = 'h'; // h stands for Horse to prevent problems with the King type
let typeBishop = 'b';
let typeQueen = 'q';
let typeKing = 'k';

// figure colors
let figWhite = 'x';
let figBlack = 'y';

// move direction operator
let opNegative = 'n';
let opPositive = 'p';
let topRight = 'tr';
let topLeft = 'tl';
let bottomRight = 'br';
let bottomLeft = 'bl';

// Object arrays
let arrFigures = [];
let arrFields = [];
let arrFieldRows = [];

// Global variables
let whitePull = true;