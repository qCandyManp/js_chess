// initializes the fields
function initField(field, index) {
	++index;

	let fieldObj = new Field(field, index);

	return fieldObj;
}

// initializes the figures
function initFigure(figure) {
	let figureObj = new Figure(figure);

	let figureType = figureObj.type;

	//create figure-object
	if (figureType == typePawn) {
		figureObj = new Pawn(figure);
	} else if (figureType == typeRook) {
		figureObj = new Rook(figure);
	} else if (figureType == typeKnight) {
		figureObj = new Knight(figure);
	} else if (figureType == typeBishop) {
		figureObj = new Bishop(figure);
	} else if (figureType == typeQueen) {
		figureObj = new Queen(figure);
	} else if (figureType == typeKing) {
		figureObj = new King(figure);
	}

	return figureObj;
};

function setFigureCssClasses (arrFigures) {
	arrFigures.forEach((figure) => {
		if (figure.htmlObj.classList.contains(figureClass)) {
			if (figure.type == typePawn) {
				figure.htmlObj.classList = '';
				figure.htmlObj.classList.add(figureClass, classCssPawn);
			} else if (figure.type == typeBishop) {
				figure.htmlObj.classList = '';
				figure.htmlObj.classList.add(figureClass, classCssBishop);
			} else if (figure.type == typeRook) {
				figure.htmlObj.classList = '';
				figure.htmlObj.classList.add(figureClass, classCssRook);
			} else if (figure.type == typeKnight) {
				figure.htmlObj.classList = '';
				figure.htmlObj.classList.add(figureClass, classCssKnight);
			} else if (figure.type == typeKing) {
				figure.htmlObj.classList = '';
				figure.htmlObj.classList.add(figureClass, classCssKing);
			} else if (figure.type == typeQueen) {
				figure.htmlObj.classList = '';
				figure.htmlObj.classList.add(figureClass, classCssQueen);
			}

			if (figure.color == figWhite) {
				figure.htmlObj.classList.add(classCssFigureWhite);
			} else if (figure.color == figBlack) {
				figure.htmlObj.classList.add(classCssFigureBlack);
			} 
		}
	});
}

// function to turn the next pull to the other color
//--- the given argument has to be a html object
function passPull () {
	let chessPlate = document.querySelector('.'+chessFieldClass+'');

	if (whitePull) {
		chessPlate.classList.add(classCssTurn);
		whitePull = false;
	} else {
		chessPlate.classList.remove(classCssTurn);
		whitePull = true;
	}
}

// sets the figures to their position
function resetFigures () {
	if (arrFields) {
		arrFields.forEach((field) => {
			let i;
			for(i = 0;i < arrFigures.length;i++) {
				if (arrFigures[i]) {
					if (arrFigures[i].fieldId != false && arrFigures[i].fieldId == field.id) {
						let attrDataType;
						let figureHtmlObj;
						if(arrFigures[i].killed) {
							attrDataType = dataType+'="'+arrFigures[i].color+arrFigures[i].type+'"';
							figureHtmlObj = '<span class="'+figureClass+'" '+attrDataType+'></span>';
							
							field.htmlObj.appendChild(figureHtmlObj);
						} else {
							attrDataType = dataType+'="'+arrFigures[i].color+arrFigures[i].type+'"';
							figureHtmlObj = '<span class="'+figureClass+'" '+attrDataType+'></span>';

							field.htmlObj.appendChild(figureHtmlObj);
						}

						break;
					}
				}
			}
		});
	}
}

let allFields = Array.prototype.slice.call(document.querySelectorAll('.'+fieldClass+'')); 
let allFigures = Array.prototype.slice.call(document.querySelectorAll('.'+figureClass+''));

allFields.forEach((field, index) => {
	let fieldObj = initField(field, index);

	fieldObj.setId;

	arrFields.push(fieldObj);
});

function setFieldRows () {
	let i;
	let idsInRow = [];

	for(i = 0; i <= arrFields.length; ++i) {
		if (arrFieldRows.length <= 8) {
			if (idsInRow.length < 8) {
				idsInRow.push(i+1);
			} else {
				arrFieldRows.push(idsInRow);
				idsInRow = [];
				idsInRow.push(i+1);
			} 
		} else {
			break;
		}
	}
}

setFieldRows();

allFigures.forEach((figure) => {
	let figureObj = initFigure(figure);
	
	arrFigures.push(figureObj);
});

arrFigures.forEach((figure) => {
	// validate the possible fields for each figure
	figure.validateFieldIds;
	figure.checkForKills;
});

setFigureCssClasses(arrFigures);

let activeFigure;

allFigures.forEach(figure => {

	figure.addEventListener('click', function (event) {

		setFigureCssClasses (arrFigures);

		let currentColor;

		if (whitePull) {
			currentColor = figWhite;
		} else {
			currentColor = figBlack;
		} 

		let figure = event.target;

		if (!figure.parentNode.classList.contains(classKillable)) {

			let testObj = initFigure(figure);
			let realObj;

			//remove class active
			let allActiveFields = Array.prototype.slice.call(document.querySelectorAll('.'+figureClass+'.'+classActive+''));

			allActiveFields.forEach((activeField) => {
				activeField.classList.remove(classActive);
			});

			figure.classList.add(classActive);

			//--- reset field
			arrFields.forEach((field) => {
				field.htmlObj.classList.remove(classValid);
				field.htmlObj.classList.remove(classKillable);

				field.valid = false;
				field.killable = false;
			});

			if (testObj.color == currentColor) {

				// figure actions
				arrFigures.forEach((figure) => {
					figure.validFieldIds = [];
					figure.possibleKills = [];

					figure.validateFieldIds;
					figure.checkForKills;
				});

				//--- get the real object from the Array
				arrFigures.forEach( (figure) => {
					if (figure.htmlObj == testObj.htmlObj) {
						realObj = figure;
					}
				});

				if (realObj !== 'undefined' && realObj) {

					let allValidFieldIds = realObj.validFieldIds;

					allValidFieldIds.forEach((validFieldId) => {
						if (validFieldId) {
							arrFields.forEach((field) => {
								if (validFieldId == field.id) {
									field.valid = true;

									field.htmlObj.classList.add(classValid);
								}
							});
						}
					});

					let allPossibleKillIds = realObj.possibleKills;

					allPossibleKillIds.forEach((possibleKillId) => {
						if (possibleKillId) {
							arrFields.forEach((field) => {
								if (possibleKillId == field.id) {
									field.killable = true;

									field.htmlObj.classList.add(classKillable);
								}
							});
						}
					});

					activeFigure = realObj;
				}
			} else {
				figure.classList.remove(classActive);
			}
		}
	});

	if (activeFigure != 'undefined' || activeFigure) {
		allFields.forEach((field) => {
			field.addEventListener('click', function (event) {
				let i;
				let fieldObj;
				let target = event.target;

				for(i = 0; i < arrFields.length; i++) {
					if (target == arrFields[i].htmlObj || target.parentNode == arrFields[i].htmlObj) {
						fieldObj = arrFields[i];
	
						break;
					}
				}
				
				if(fieldObj && fieldObj.valid || fieldObj && fieldObj.killable) {
					
					if (activeFigure != 'undefined' && activeFigure && arrFigures.includes(activeFigure)) {
						
						if(fieldObj.killable) {
							let i;

							for(i = 0;i < arrFigures.length;i++) {
								if (arrFigures[i] && arrFigures[i].fieldId != false) {
									if (arrFigures[i].fieldId == fieldObj.id) {
										arrFigures[i].htmlObj.remove();
										arrFigures[i].killed = true;
										arrFigures[i].fieldId = false;
									
										break;
									}
								}
							}
						}

						// Function to move a Figure
						function moveFigure (figure, field) {
							figure.htmlObj.remove();
							field.htmlObj.appendChild(figure.htmlObj);

							//--- reset the figure object for its new position 
							figure.fieldId = field.id;
							figure.resetFigureInfo;
							figure.validateFieldIds;
							figure.checkForKills;

							if (figure.moved && figure.moved > 0) {
								figure.moved = parseInt(figure.moved);
								figure.moved = ++figure.moved;
							} else {
								figure.moved = 1;
							}
						}

						// King special moves
						if (activeFigure.type == typeKing && !activeFigure.moved || activeFigure.type == typeKing && activeFigure.moved <= 0) {
							if (activeFigure.rochadeIds.includes(fieldObj.id)) {  
								let newRookField;
								let newRookFieldId;
								
								if (fieldObj.id > activeFigure.fieldId) {
									let i;
									for(i = 0;i < arrFigures.length;i++) {
										if (arrFigures[i] && arrFigures[i].fieldId != false) {
											if (parseInt(arrFigures[i].fieldId) == 64 || parseInt(arrFigures[i].fieldId) == 8) {
												if (arrFigures[i].type == typeRook && arrFigures[i].color == activeFigure.color) {
													newRookFieldId = parseInt(activeFigure.fieldId) + 1;
													let num;
													for(num = 0;num < arrFields.length;num++) {
														if (arrFields[num].id == newRookFieldId) {
															newRookField = arrFields[num];
															break;
														}
													}
													moveFigure (arrFigures[i], newRookField);
													break;
												}
											}
										}
									}
								} else if (fieldObj.id < activeFigure.fieldId) {
									let i;
									for(i = 0;i < arrFigures.length;i++) {
										if (arrFigures[i] && arrFigures[i].fieldId != false) {
											if (parseInt(arrFigures[i].fieldId) == 57 || parseInt(arrFigures[i].fieldId) == 1) {
												if (arrFigures[i].type == typeRook && arrFigures[i].color == activeFigure.color) {
													newRookFieldId = parseInt(activeFigure.fieldId) - 1;
													let num;
													for(num = 0;num < arrFields.length;num++) {
														if (arrFields[num].id == newRookFieldId) {
															newRookField = arrFields[num];
															break;
														}
													}

													moveFigure (arrFigures[i], newRookField);
													break;
												}
											}
										}
									}
								}

								moveFigure (activeFigure, fieldObj);
							} else {
								moveFigure (activeFigure, fieldObj);
							}

						// Pawn special moves
						} else if (activeFigure.type == typePawn) {
							moveFigure (activeFigure, fieldObj);

							let allFigureSelects = Array.prototype.slice.call(document.querySelectorAll('.'+figureSelectClass+''));

							function changePawn (activeFigure) {
								allFigureSelects.forEach((figureSelect) => {
									figureSelect.addEventListener('click', function (event) {
										event.preventDefault();

										let target = event.target;
										let targetType = target.getAttribute(dataType);
										let currentPawn = activeFigure.htmlObj;
										let newFigure;

										if (targetType == typeBishop || targetType == typeKnight || targetType == typeQueen || targetType == typeRook) {
											currentPawn.setAttribute(dataType, activeFigure.color+targetType);

											let i;
											for(i = 0;i < arrFigures.length;i++) {
												if (arrFigures[i] && arrFigures[i].htmlObj == currentPawn) {
													delete arrFigures[i];

													break;
												}
											}

											newFigure = initFigure(currentPawn);
											arrFigures.push(newFigure);
											setFigureCssClasses(arrFigures);

											document.querySelector('.'+figureSelectOverlayClass+'').classList.remove(classActive);
										}
									});
								});
							}
							
							if (activeFigure.color == figWhite && activeFigure.fieldRowId == 0) {
								document.querySelector('.'+figureSelectOverlayClass+'').classList.add(classActive);
								changePawn(activeFigure);
							} else if (activeFigure.color == figBlack && activeFigure.fieldRowId == 7) {
								document.querySelector('.'+figureSelectOverlayClass+'').classList.add(classActive);
								changePawn(activeFigure);
							}

						// default Figure moves
						} else {
							moveFigure (activeFigure, fieldObj);
						}

						//--- reset fields
						arrFields.forEach((field) => {
							field.htmlObj.classList.remove(classValid);
							field.htmlObj.classList.remove(classKillable);

							field.valid = false;
							field.killable = false;
						});

						arrFigures.forEach((figure) => {
							if (figure.htmlObj.classList.contains(classActive)) {
								figure.htmlObj.classList.remove(classActive);
							}
						});

						//--- turn the field
						passPull();
					}
				}
			});
		});
	}
});